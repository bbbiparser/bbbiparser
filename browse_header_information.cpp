#include "read_big/BigHeader.h"
#include "read_big/ZoomHeader.h"
#include "read_big/TotalSummary.h"
#include "read_big/ChromosomeBTree.h"
#include "read_big/R_Tree.h"
#include "read_big/R_TreeNode.h"
#include "read_big/BigWigSegment.h"
#include "read_big/BigFile.h"
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>

// read in a file of file names and parse the header
// structures in these files using the BigHeader class.
// print out some information regarding the different files

// this written mainly to test the BigHeader function and
// to go through other things. Have to bring things together
// later on.

int main(int argc, char** argv){
  // let's try with BigFile.. 


  std::ifstream in(argv[1]);
  if(!in){
    std::cerr << "Bugger unable to open " << argv[1] << std::endl;
    exit(1);
  }

  int repeat_offender = 0;
  if(argc > 2)
    repeat_offender = atoi(argv[2]);

  // read file names from the file using getline (string function)
  std::vector<std::string> file_names;
  std::string file_name;
  while( getline( in, file_name ))
    file_names.push_back(file_name);
  std::cout << "Obtained a total of " << file_names.size() << " files" << std::endl;
  in.close();
  
  for(unsigned int i=0; i < file_names.size(); ++i){
    std::cout << "Making a big file from: " << file_names[i] << std::endl;
    BigFile bigFile(file_names[i].c_str());
    continue;
    

    std::ifstream in2( file_names[i].c_str() );
    if(!in2.good()){
      std::cerr << "Unable to open file " << i << "\t" << file_names[i] << std::endl;
      in2.close();
      continue;
    }
    BigHeader* bigh = new BigHeader(in2); // = new BigHeader(in2);
    // and then
    std::cout << "Summary for : " << file_names[i] << std::endl;
    bigh->print_summary();

    // and lets get zoom level information.
    // after reading the file stream should be immediately after
    // the zoom header, but let's not assume that.
    in2.seekg(BIG_HEADER_SIZE);
    if(!in2.good()){
      std::cerr << "Unable to set stream to end of big header " << i << "\t" << file_names[i] << std::endl;
      continue;
    }
    for(unsigned int j=0; j < bigh->zoomLevels; ++j){
      //ZoomHeader* zh = new ZoomHeader(in2, bigh->reverse_bytes);
      // zh->print_summary();
    }
    // if totalSummaryOffset, read a total offset..
    if(bigh->totalSummaryOffset){
      in2.seekg(bigh->totalSummaryOffset);
      if(!in2.good()){
	std::cerr << "Unable to seek to totalSummaryOffset: "
		  << bigh->totalSummaryOffset << std::endl;
      }else{
	//TotalSummary* t_sum = new TotalSummary(in2, bigh->reverse_bytes);
	//t_sum->print_summary();
      }
    }
    // read the chromsome tree: ??
    //std::cout << "Seeking to position: " << bigh->chromosomeTreeOffset << std::endl;
    in2.seekg(bigh->chromosomeTreeOffset);
    if(in2.good()){
      //      std::cout << "creating a chrom b tree ? " << std::endl;
      //ChromosomeBTree* cbt = new ChromosomeBTree(in2);
      //cbt->print_summary();
    }else{
      std::cerr << "Unable to seek to the beginning of the Chromosome file tree\n"
		<< "\tpos: " << bigh->chromosomeTreeOffset << std::endl;
    }
    // read the full index
    //std::cout << "Seeking to position: " << bigh->fullIndexOffset << std::endl;
    in2.seekg(bigh->fullIndexOffset);
    if(!in2.good()){
      std::cerr << "Unable to seek to the beginning of the full index: " 
		<< bigh->fullIndexOffset << std::endl;
    }else{
      R_Tree* index = new R_Tree(in2);
      index->print_summary();

      // lets get an arbitrary node on chrom 1.. 
      R_TreeSubNode* subNode = index->getLeafNode(1, 85000000);
      // 

      if(!subNode){
	std::cerr << "Oh bugger, unable to get a leaf node ??" << std::endl;
      }else{
	subNode->print_summary(1);
	// and let's try to get the actual data.
	if(!bigh->isBigBed){
	  clock_t time = clock();
	  std::cout << "making a bigwigsegment " << repeat_offender << " times"  << std::endl;
	  for(int i=0; i < repeat_offender; ++i){
	    BigWigSegment* bws = new BigWigSegment(in2, subNode, bigh);
	    //bws->print_summary();
	    //bws->print_data();
	    delete bws;
	  }
	  clock_t time_taken = clock() - time;
	  std::cout << "Repeated reading bigwig segment : " << repeat_offender << " times "
		    << " Time elapsed = " << float(time_taken) / CLOCKS_PER_SEC << " seconds "
		    << std::endl;
	  
	}
      }
      unsigned int counter = 0;
      clock_t time = clock();
      for(int i=0; i < repeat_offender; ++i){
	subNode = index->getLeafNode(i % 20,  6000000 + i );
	if(subNode) ++counter;
      }
      clock_t time_taken = clock() - time;
      std::cout << "Received a node " << counter << " / " << repeat_offender << std::endl;
      std::cout << "Looking up " << repeat_offender << " leaf nodes took "
		<< float(time_taken)/CLOCKS_PER_SEC << " seconds" << std::endl;
      if(subNode) subNode->print_summary(2);
      
      // let's try to get lots of subnodes in one go.
      std::vector<R_TreeSubNode*> nodes;
      index->getLeafNodes(1, 85000000, 1, 88000000, nodes);
      std::cout << "index returned " << nodes.size() << " nodes for: " << 88000000-85000000 << " bases" << std::endl;
      for(unsigned int i=1; i < nodes.size(); ++i){
	//nodes[i]->print_summary(3);
	//std::cout << "Distance between nodes: " << nodes[i]->startBase - nodes[i-1]->endBase << std::endl;
      }
    }
  }
}
